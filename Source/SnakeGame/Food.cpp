// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponentFood = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponentFood"));
	MeshComponentFood->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponentFood->SetCollisionResponseToAllChannels(ECR_Overlap);
	
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{		
			if (Virus)
			{				
				//Destroy();									������ ����� ��
				Snake->DeleteSnakeElement();				
			}
			if (SpeedUp)
			{				
				//Destroy();
				Snake->SpeedUpSnake();				
			}
			if (SpeedDown)
			{				
				//Destroy();
				Snake->SpeedDownSnake();				
			}
			if (!SpeedDown & !SpeedUp & !Virus)
			{				
				//Destroy();
				Snake->AddSnakeElementBonus();				
			}
		}		
	}
}

