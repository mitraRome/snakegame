// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"



// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 70.f;
	MovementSpeed = 0.5f;
	LastMoveDirection = EMovementDirection::LEFT;
	PreMoveDirection = EMovementDirection::LEFT;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	
	AddSnakeElement(2);	
	
}
// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(MovementSpeed);
	Move();
	//GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::White, FString);
	
}


void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);		
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();			
		}
	}
}
void ASnakeBase::AddSnakeElementBonus() // ����� ��������� ������
{

	FTransform NewTransform(LastPosition);
	ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	NewSnakeElem->SnakeOwner = this;
	SnakeElements.Add(NewSnakeElem);	
}
void ASnakeBase::DeleteSnakeElement() // �������� ������
{	
	if (SnakeElements.Num()-1 > 0)
	SnakeElements.RemoveAt(SnakeElements.Num() - 1);		
}
void ASnakeBase::SpeedUpSnake()  
{
	if (MovementSpeed > 0.2f)
		MovementSpeed-= 0.1f;
}
void ASnakeBase::SpeedDownSnake()
{
	if (MovementSpeed < 0.7f)
	MovementSpeed += 0.1f;
}


void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
			MovementVector.X += ElementSize; 
			PreMoveDirection = EMovementDirection::UP;
			break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize; 
		PreMoveDirection = EMovementDirection::DOWN;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize; 
		PreMoveDirection = EMovementDirection::LEFT;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize; 
		PreMoveDirection = EMovementDirection::RIGHT;
		break;
	}
	
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CorrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		LastPosition = PrevLocation;
		CorrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}



