// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "Components/StaticMeshComponent.h"
#include "FoodStatic.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API UFoodStatic : public UStaticMeshComponent, public IInteractable
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Settings Food")
		bool Virus;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Settings Food")
		bool SpeedUp;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Settings Food")
		bool SpeedDown;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;



};

