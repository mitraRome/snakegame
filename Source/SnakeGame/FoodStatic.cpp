// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodStatic.h"
#include "SnakeBase.h"





UFUNCTION(BlueprintCallable)
void UFoodStatic::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		
		if (IsValid(Snake))
		{
			if (Virus)
			{
				DestroyComponent();
				Snake->DeleteSnakeElement();
			}
			if (SpeedUp)
			{
				DestroyComponent();
				Snake->SpeedUpSnake();
			}
			if (SpeedDown)
			{
				DestroyComponent();
				Snake->SpeedDownSnake();
			}
			else
			{
				DestroyComponent();
				Snake->AddSnakeElementBonus();
			}
		}
	}
}
